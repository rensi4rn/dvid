# Create your views here.
# -*- coding: utf-8 -*-
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, loader
from appmedia.models import Video
from django.shortcuts import render_to_response, get_object_or_404
from django.core.paginator import Paginator



import json


def index(request):
    videos = Video.objects.all()
   
    return render_to_response('index.html',{}, context_instance=RequestContext(request))


def some_view(request):
    result = {}
    result["total"]=12
    #result.append({"user":request.user})
    #result.append({"key":232})
    return HttpResponse(json.dumps(result), mimetype='application/json')


def videojson(request):
    
    try:
        #Get browser parameters
        intLimit = int(request.GET.get('limit',0))
        intPage = int(request.GET.get('page',0))
        strQuery = request.GET.get('query','')
        
        #Verify if exists a filter
        if strQuery!='':
            strQueryVal = strQuery.split('#')
            #Apply filter by column   
            if strQueryVal[1]!='':
                #Filter data
                if strQueryVal[0]=='title':
                    objVideos = Video.objects.filter(title__contains=strQueryVal[1])
                elif strQueryVal[0]=='video_name':
                    objVideos = Video.objects.filter(video_name__contains=strQueryVal[1])
                elif strQueryVal[0]=='description':
                    objVideos = Video.objects.filter(description__contains=strQueryVal[1])
                else:
                    objVideos = Video.objects.all()    
            else:
                #without filter
                objVideos = Video.objects.all()
        else:
            objVideos = Video.objects.all()
        
        #Paginate video objects
        objPag = Paginator(objVideos,intLimit)
        objVideosResult = objPag.page(intPage).object_list
        intCount=objPag.count
        
        #Build result
        result = {
                  'totalCount':intCount,
                  'topics':[video.json() for video in objVideosResult]}
        #Send result 
        return HttpResponse(json.dumps(result), content_type='application/json')
    except:
        result = {
                  'totalCount':0,
                  'topics':[]}













