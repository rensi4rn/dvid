from django.contrib import admin
from appmedia.models import Video


class VideoAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                  {'fields': ['title']}),
        ('Video information',   {'fields': ['video_name','video_type','description','file','upload_date']}),
        ('Image display',       {'fields':['poster']})
    ]
    list_display = ('title','upload_date', 'was_published_recently')
    list_filter = ['upload_date','video_type']
    search_fields = ['title','description','upload_date']
    date_hierarchy = 'upload_date'

admin.site.register(Video,VideoAdmin)