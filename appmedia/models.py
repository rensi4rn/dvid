import datetime
import os
from django.db import models
from django.utils import timezone
from django.core.validators import ValidationError  

# Create your models here.
class Video(models.Model):
    
    VIDEO_TYPE_CHOICES = ( 
        ('video/mp4','mp4'), 
        ('video/quicktime','mov'), 
    ) 
    
    
    title = models.CharField(max_length=50)
    video_name = models.CharField(max_length=100)
    description = models.CharField(max_length=1000)
    file = models.FileField(upload_to = 'files')
    upload_date = models.DateTimeField('date upload')
    video_type = models.CharField("Video Type",max_length=70,choices=VIDEO_TYPE_CHOICES)
    poster = models.ImageField(upload_to='posters', verbose_name='Poster')
    
    def validate_unique(self, *args, **kwargs):
        super(Video, self).validate_unique(*args, **kwargs)
        file = self.file
        if file:
            if file.size > 250*1024*1024:
                raise ValidationError({'file':("Video file too large ( > 250 mb )",)})
            if not os.path.splitext(file.name)[1] in [".mp4",".mov"]:
                raise ValidationError({'file': ("Only '.mp4' and '.mov' files are allowed.",)})
            
        else:
            raise ValidationError({'file': ("Couldn't read uploaded file",)})
    
    
    def json(self):
        return {
             'id':self.id,  
             'title':self.title,
             'video_name':self.video_name,
             'description':self.description,
             'src':self.file.url,
             'srcposter':self.poster.url,
             'upload_date':self.upload_date.strftime('%Y-%m-%d %H:%M:%S'), 
             'videotype':self.video_type
                
        }
    
    def __str__(self):
        return self.title
    
    def was_published_recently(self):
        return self.upload_date>= timezone.now() - datetime.timedelta(days=1)