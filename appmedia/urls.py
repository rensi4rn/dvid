from django.conf.urls import patterns, url
from appmedia import views

urlpatterns = patterns('',
    url(r'^videojson/$','appmedia.views.videojson'),
    url(r'^some_view/$','appmedia.views.some_view'),
    url(r'^$', views.index, name='index') 
)